#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

// sprite arrays
char *awake[]={" /\\---/\\\n| _  _ |\n|  -   |\n+------+\n", " /\\---/\\\n| o  o |\n|  -   |\n+------+\n", " /\\---/\\\n| o  o |\n|  W   |\n+------+\n"};
char *asleep[]={"    Z z z\n/\\---/\\\n| -_ - |\n+------+\n", "    z Z z\n/\\---/\\\n| -_ - |\n+------+\n", "    z z Z\n/\\---/\\\n| -_ - |\n+------+\n"};

int killcurses(){
	nocbreak();
	endwin();
	delscreen(stdscr);
}

int rng(int min, int max){
	FILE *fp;
	char seed;
	int i;
	fp = fopen("/dev/urandom", "r");
	fread(&seed, 1, 1, fp);
	fclose(fp);
	srand(seed);
	i = rand() % (max-min) + min;
	return i;
}

int animate(int ss){
	move(2,0);
	if(ss == 0)
		printw("%s", awake[rng(0,3)]);
	if(ss == 1)
		printw("%s", asleep[rng(0,3)]);
}

int statusbar(int hung, int tird, int hyg){
	move(1,0);
	printw("Hunger: [%d/100] | Tiredness: [%d/100] | Hygeine: [%d/100]\n", hung, tird, hyg);
}

int bed(int hung, int tird, int hyg){
	for(tird; tird<100; tird++){
		if(getch() == 'q'){
			system("reset");
			exit(0);
		}
		sleep(1);
		statusbar(hung, tird, hyg);
		animate(1);
		refresh();
	}

	return tird;

};

int main(){
	// init ncurses
	initscr();
	raw(); // disable line buffering, don't interpret control codes as chars
	noecho();
	timeout(10); // input timeout
	curs_set(0);

	// init vars
	int hung = 100;
	int tird = 100;
	int hyg = 100;

	printw("(F)ood | (C)lean | (B)ed | (Q)uit\n");
	statusbar(hung, tird, hyg);
	animate(0);
	refresh();

	// control loop
	int ch;


	while(1){
		ch = getch();
		if(ch == 'f'){
			if(hung < 100){
				hung = hung + 10;
			}
		} else if(ch == 'c'){
			if(hyg > 0){
				hyg = hyg + 10;
			}
		} else if(ch == 'b'){
			if(tird < 100){
				tird = bed(hung, tird, hyg);
			}
		} else if(ch == 'q'){
			killcurses();
			exit(0);
		} else{
			// tick
			if(rng(0,2) == 1)
				hung = hung - rng(0,6);
			if(rng(0,2) == 1)
				hyg = hyg - rng(0,6);
			if(rng(0,2) == 1)
				tird = tird - rng(0,6);
			// check for death
			if(hung <= 0 || hyg <= 0 || tird <= 0){
				killcurses();
				printf("Your pet died from neglect\n");
				exit(0);
			} else if (hung > 100){
				killcurses();
				printf("You fed your pet to death\n");
				exit(0);
			} else if (hyg > 100){
				killcurses();
				printf("You cleaned your pet to death\n");
				exit(0);
			}




			}

			sleep(1);
			animate(0);
			statusbar(hung, tird, hyg);
			refresh();
		}

	}
